IMAGE_NAME = centos
IMAGE_VERSION = 7
IMAGE_VERSION_ALIASES =	7
IMAGE_TITLE = CentOS 7
IMAGE_DESCRIPTION = CentOS 7
IMAGE_SOURCE_URL = https://gitlab.com/geoffrey-grebert.com/docker2rootfs
IMAGE_VENDOR_URL = https://www.centos.org/
IMAGE_BOOTSCRIPT =	mainline 4.4
IMAGE_BASE_FLAVORS = systemd common docker-based

