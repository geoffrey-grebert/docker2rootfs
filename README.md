# Centos docker image to rootfs

**Warning: this is image is still in development**

Scripts to build the official Docker Centos image to rootfs

This image is built using [Image Tools](https://github.com/scaleway/image-tools) and depends on the [offical centos](https://registry.hub.docker.com/_/centos/) Docker image.

<img src="https://upload.wikimedia.org/wikipedia/commons/b/bc/Centos_full.svg" width="450px" />


## Install (tested on Ubuntu 18.04 LTS)

Install dependencies:

```bash
sudo apt -y install \
  docker.io \
  git \
  build-essential
```

Download project:

```bash
git clone https://github.com/scaleway/image-tools
git clone https://gitlab.com/geoffrey-grebert/docker2rootfs
```

Build:

```bash
cd image-tools
make IMAGE_DIR=$PWD/../docker2rootfs/centos rootfs.tar
```

